FROM golang:1.22.1-alpine3.19

RUN apk add git

ADD --keep-git-dir=true https://gitlab.com/musys-demo/hello_go.git /hello_go

WORKDIR /hello_go

RUN git checkout main

RUN go mod download && go mod verify

RUN go install

RUN go build